# Sinology Extra
Nicht prüfungsrelevante Inhalte für Sinologieinteressierte.

## Download
Die neueste Version der APKG-Dateien gibt es unter
[Releases](https://gitlab.phaidra.org/kartenaale/packs/sinology-extra/-/releases).

Importiere sie in Anki um loszulegen. Wenn dir Anki noch neu ist, kannst du
auch die _Anleitung für neue Benutzer:innen_ auf
[Englisch](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/GUIDE.md)
oder
[Deutsch](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/ANLEITUNG.md)
zurateziehen.

## Was ist enthalten?
* `Chinese-Hand-Gestures-2.0.3.apkg`: Handgesten für Zahlen

## Dieses Pack verbessern
Wenn du Fehler in diesem Pack oder in anderen findest, oder wenn du eigene
Anki-Inhalte hast die du teilen möchtest, melde dich auf
[Whatsapp](https://chat.whatsapp.com/JFKpfmq29yM2xKcSu7JQib).
Wenn du einen Gitlab-Account hast, kannst du auch ein
[Issue anlegen](https://gitlab.phaidra.org/kartenaale/packs/sinology-extra/-/issues/new).
